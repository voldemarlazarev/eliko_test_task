#ifndef TAGSTATE_H
#define TAGSTATE_H
#include <cstdint>
#include "xyzcoordinates.h"

namespace Tag {

using Id = uint32_t;
using TimestampMs = uint32_t;
using Coord = double;

struct State {
    State(Id id,
          TimestampMs timestamp,
          XyzCoordinates<Coord> coords,
          bool moving);


    Id id;
    TimestampMs timestamp;
    XyzCoordinates<Coord> coords;
    bool moving;
};

}

#endif // TAGSTATE_H
