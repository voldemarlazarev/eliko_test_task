#include "tagstate.h"


Tag::State::State(Id id, TimestampMs timestamp, XyzCoordinates<Coord> coords, bool moving)
    : id{id},
      timestamp{timestamp},
      coords{coords},
      moving{moving}
{}
