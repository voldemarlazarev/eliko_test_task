#include "packetreader.h"
#include <sstream>
#include <cstdint>
#include <iostream>

PacketReader::PacketReader()
{

}

template <typename StartIterT, typename EndIterT>
class ValueGetter {
public:
    ValueGetter(StartIterT startIter, EndIterT endIter)
        : iterator{startIter},
          end{endIter}
    {

    }

    typename StartIterT::reference getAndIncrementIterator()
    {
        if (iterator == end)
            throw std::runtime_error{"Iterator reached an end"};

        return *iterator++;
    }

private:
    StartIterT iterator;
    EndIterT end;
};

Packets::Packet PacketReader::readPacket(const std::string &line)
{
    // Split by lines
    const char delimiter = ',';
    std::vector<std::string> items;

    std::stringstream ss{line};

    std::string lineRead;
    while(std::getline(ss, lineRead, delimiter)) {
        items.push_back(lineRead);
    }

    // ToDo: check if exception is thrown if wrong index is accessed at items

    using namespace Packets;
    if (items.size() >= 2
            && items[0] == "$PEKIO") {
        const auto& type {items[1]};

//        decltype(items)::const_iterator fieldsIter = items.cbegin() + 2;
        ValueGetter fieldsIter{items.cbegin() + 2, items.cend()};
        if (type == "RR_L") {
            Packets::byte sequenceNr = decodeSequenceNr(fieldsIter.getAndIncrementIterator());
            Packets::device_id deviceId = decodeDeviceId(fieldsIter.getAndIncrementIterator());

            vector<AnchorIdDistance> anchorDistances;
            anchorDistances.reserve(3);

            std::string encodedField;
            unsigned int nAnchors = 0;
            while(isEncodedAnchorId(encodedField = fieldsIter.getAndIncrementIterator())) {
                auto&& anchorId = decodeAnchorId(encodedField);
                auto&& distance = decodeDistance(fieldsIter.getAndIncrementIterator());
                anchorDistances.emplace_back(anchorId, distance);
                ++nAnchors;
            }
            timestamp timestamp = decodeTimestamp(encodedField);

            vector<byte> values;
            for(unsigned int i = 0; i < nAnchors; i++) {
                values.push_back(decodeByte(fieldsIter.getAndIncrementIterator()));
            }

            byte moving = decodeMoving(fieldsIter.getAndIncrementIterator());

            return Packet_RR_L {
                .sequenceNr = std::move(sequenceNr),
                .deviceId = std::move(deviceId),
                .anchorDistances = std::move(anchorDistances),
                .timestamp = std::move(timestamp),
                .values = std::move(values),
                .moving = moving
            };
        }
        else if (type == "COORD") {
            uint8_t sequenceNr = decodeSequenceNr(fieldsIter.getAndIncrementIterator());
            Packets::tag_id tagId = decodeHex(fieldsIter.getAndIncrementIterator());
            coord xCoord = decodeCoord(fieldsIter.getAndIncrementIterator());
            coord yCoord = decodeCoord(fieldsIter.getAndIncrementIterator());
            coord zCoord = decodeCoord(fieldsIter.getAndIncrementIterator());
            std::string errorMessage = std::move(fieldsIter.getAndIncrementIterator());
            uint8_t value = decodeCoordPacketValue(fieldsIter.getAndIncrementIterator());

            return Packet_COORD{
                .sequenceNr = std::move(sequenceNr),
                .tagId = std::move(tagId),
                .x = std::move(xCoord),
                .y = std::move(yCoord),
                .z = std::move(zCoord),
                .errorMessage = std::move(errorMessage),
                .value = std::move(value)
            };
        }
        else {
            // ToDo
            return Packet_COORD{};
        }
    }
}
