#ifndef TAGDATAPROCESSOR_H
#define TAGDATAPROCESSOR_H
#include "tagstate.h"
#include <optional>

//ToDo: move to units header file
using Speed = double;

class TagDataProcessor {
public:
    TagDataProcessor();
    void processTagState(const Tag::State& state);

private:
    class SpeedCalculator {
    public:
        void processState(const Tag::State &state)
        {
            // Calculate speed
            if (!state.moving) {
                prevState.reset();
                return;
            }

            if (state.moving) {
                if (prevState.has_value() && prevState->moving) {
                    double deltaTimeMs = state.timestamp - prevState->timestamp;
                    double deltaTimeSec = deltaTimeMs / 1000.0;
                    double deltaCoords = coordinateDistance(state.coords, prevState->coords);
                    speed = deltaCoords / deltaTimeSec;
                }

                prevState = state;
            }
            else {
                prevState.reset();
            }

            prevState = state;
        }

        bool hasSpeed()
        {
            return speed.has_value();
        }

        Speed getSpeed()
        {
            return *speed;
        }

    private:
        std::optional<Tag::State> prevState;
        std::optional<Speed> speed;
    };

    class MaxSpeedWhenMovingCalculator {
    public:
        struct SpeedAtPeriod {
            // start <= time < end
            Tag::TimestampMs timestampStart;
            Tag::TimestampMs timestampEnd;
            Speed speed;
        }

        processTagState(const Tag::State& state)
        {

        }

    private:
    };

    MaxSpeedWhenMovingCalculator maxSpeedCalc;
//    AverageSpeedWhenMovingCalculator avgSpeedCalc;
};

#endif // TAGDATAPROCESSOR_H
