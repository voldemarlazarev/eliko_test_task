#ifndef PACKETREADER_H
#define PACKETREADER_H

#include <iosfwd>
#include "packets.h"

class PacketReader
{
public:
    PacketReader();

    Packets::Packet readPacket(const std::string& line);

};

#endif // PACKETREADER_H
