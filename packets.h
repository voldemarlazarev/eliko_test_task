#ifndef PACKETS_H
#define PACKETS_H

#include <cstdint>
#include <vector>
#include <string>
#include <variant>

namespace Packets {
using byte = uint8_t;
using device_id = uint32_t;
using tag_id = uint32_t;
using timestamp = uint32_t;
using coord = double;
using string = std::string;
using std::vector;

struct AnchorIdDistance {
    AnchorIdDistance(int anchorId, int distance)
        : anchorId{anchorId},
          distance{distance}
    {}

    int anchorId;
    int distance;
};

bool isEncodedHex(const std::string& encoded);

inline int decodeHex(const std::string& encoded)
{
    return std::stoi(encoded, nullptr, 16);
}

inline byte decodeByte(const std::string& encoded)
{
    return std::stoi(encoded);
}

inline device_id decodeSequenceNr(const std::string& encoded)
{
    return std::stoi(encoded);
}

inline device_id decodeDeviceId(const std::string& encoded)
{
    return decodeHex(encoded);
}

inline device_id decodeTagId(const std::string& encoded)
{
    return decodeHex(encoded);
}

inline timestamp decodeTimestamp(const std::string& encoded)
{
    return std::stoi(encoded);
}

inline coord decodeCoord(const std::string& encoded)
{
    return std::stod(encoded);
}

inline bool isEncodedAnchorId(const std::string& encoded)
{
    return isEncodedHex(encoded);
}

inline int decodeAnchorId(const std::string& encoded)
{
    return decodeHex(encoded);
}

inline int decodeDistance(const std::string& encoded)
{
    return std::stoi(encoded);
}

inline float decodeCoordPacketValue(const std::string& encoded)
{
    return std::stof(encoded);
}

byte decodeMoving(const std::string& encoded);


// Packet types do not inherit from single packet type because other packets may have different structure

struct Packet_RR_L {
    byte sequenceNr;
    device_id deviceId;
    vector<AnchorIdDistance> anchorDistances;
    timestamp timestamp;
    vector<byte> values;
    byte moving;
};

struct Packet_COORD {
    byte sequenceNr;
    tag_id tagId;
    coord x;
    coord y;
    coord z;
    std::string errorMessage;
    int value;
    bool isGood() const;
};

using Packet = std::variant<Packet_RR_L, Packet_COORD>;

}

#endif // PACKETS_H
