#ifndef TAGSTATEBUILDER_H
#define TAGSTATEBUILDER_H
#include "packets.h"
#include "tagstate.h"
#include <optional>


class TagStateBuilder {
public:
    void addPacket(const Packets::Packet_COORD& packet);
    void addPacket(const Packets::Packet_RR_L& packet);
    bool isReadyToBuild() const;
    Tag::State build() const;
    void reset();

private:
    std::optional<Packets::Packet_RR_L> rrlPacket;
    std::optional<Packets::Packet_COORD> coordPacket;

    struct BuildablePacketHeader {
        Packets::byte sequenceNr;
        Tag::Id tagId;
    };

    std::optional<BuildablePacketHeader> buildablePacketHeader;

    void resetIfRequired(const BuildablePacketHeader &header);
};

#endif // TAGSTATEBUILDER_H
