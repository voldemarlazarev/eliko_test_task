#ifndef DATAPROCESSOR_H
#define DATAPROCESSOR_H

#include "tagstatebuilder.h"
#include "packets.h"
#include "tagdataprocessor.h"
#include <iostream>
#include <unordered_map>

class DataProcessor {
public:
    void processPacket(const Packets::Packet_COORD &packet);
    void processPacket(const Packets::Packet_RR_L &packet);

    void printResult()
    {
        using std::cout, std::endl;
        cout << "coord: " << coordPacketCount << endl;
        cout << "rrl: " << rrlPacketCount << endl;
    }

private:
    void calculate();

    struct SampleStats {
        int coordPacketCount{0};
        int rrlPacketCount{0};

        int badSampleCount{0};
        int goodSampleCount{0};
    } sampleStats;

    TagStateBuilder tagStateBuilder;

    void updateSampleStats(const Packets::Packet_COORD &packet);
    void finishPacketProcessing();
    void calculateValues();

    std::unordered_map<Tag::Id, TagDataProcessor> tagDataProcessors;
};

#endif // DATAPROCESSOR_H
