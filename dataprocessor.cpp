#include "dataprocessor.h"


void DataProcessor::processPacket(const Packets::Packet_COORD& packet)
{
    updateSampleStats(packet);
    tagStateBuilder.addPacket(packet);

    ++sampleStats.coordPacketCount;
}

void DataProcessor::processPacket(const Packets::Packet_RR_L& packet)
{

    ++sampleStats.rrlPacketCount;

    tagStateBuilder.addPacket(packet);
}

void DataProcessor::updateSampleStats(const Packets::Packet_COORD& packet)
{
    if (!packet.errorMessage.empty())
        ++sampleStats.badSampleCount;
    else
        ++sampleStats.goodSampleCount;
}

void DataProcessor::finishPacketProcessing()
{
    if (!tagStateBuilder.isReadyToBuild())
        return;

    Tag::State tagState = tagStateBuilder.build();
    tagStateBuilder.reset();

    // ToDo: make processing in parallel

    auto tagId = tagState.id;

    tagDataProcessors[tagId].processTagState(tagState);
}



