#ifndef XYZCOORDINATES_H
#define XYZCOORDINATES_H

#include <cmath>

template <typename T>
struct XyzCoordinates {
    T x;
    T y;
    T z;
};

template <typename T>
T coordinateDistance(const XyzCoordinates<T>& a, const XyzCoordinates<T>& b)
{
    auto dx = a.x - b.x;
    auto dy = a.y - b.y;
    auto dz = a.z - b.z;

    using std::pow;
    using std::sqrt;
    return sqrt(pow(dx, 2) + pow(dy, 2) + pow(dz, 2));
}

#endif // XYZCOORDINATES_H
