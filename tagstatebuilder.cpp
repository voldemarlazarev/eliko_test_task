#include "tagstatebuilder.h"
#include "tagstate.h"
#include <stdexcept>

void TagStateBuilder::addPacket(const Packets::Packet_COORD &packet)
{
    resetIfRequired({packet.sequenceNr, packet.tagId});
}

void TagStateBuilder::addPacket(const Packets::Packet_RR_L &packet)
{
    resetIfRequired({packet.sequenceNr, packet.deviceId});
}

bool TagStateBuilder::isReadyToBuild() const
{
    return rrlPacket.has_value() && coordPacket.has_value();
}

Tag::State TagStateBuilder::build() const
{
    if (!isReadyToBuild())
        throw std::runtime_error{"Not data to build TagState"};

    using namespace Tag;
    Id tagId = coordPacket->tagId;
    TimestampMs timestamp = rrlPacket->timestamp;
    XyzCoordinates<Coord> coords{coordPacket->x, coordPacket->y, coordPacket->z};
    bool moving = (rrlPacket->moving != 1);

    return State{tagId, timestamp, coords, moving};
}

void TagStateBuilder::reset()
{
    this->rrlPacket.reset();
    this->coordPacket.reset();
    this->buildablePacketHeader.reset();
}

void TagStateBuilder::resetIfRequired(const BuildablePacketHeader& header)
{
    if (buildablePacketHeader.has_value()
            && header.sequenceNr == buildablePacketHeader->sequenceNr
            && header.tagId == buildablePacketHeader->tagId)
        return;

    buildablePacketHeader = header;
    reset();
}
