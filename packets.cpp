#include "packets.h"
#include <stdexcept>

bool Packets::isEncodedHex(const std::string &encoded)
{
    if (!(encoded.size() >= 3))
        return false;

    char xSymbol = encoded.at(1);
    return encoded.at(0) == '0' && (xSymbol == 'x' || xSymbol == 'X');
}

Packets::byte Packets::decodeMoving(const std::string &encoded)
{
    auto&& number = decodeHex(encoded);
    if (!(number >= 0 && number <= 1))
        throw std::invalid_argument{std::string{"Value not allowed: "} + std::to_string(number)};
    return number;
}

bool Packets::Packet_COORD::isGood() const
{
    return errorMessage.empty();
}
